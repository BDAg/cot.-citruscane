# Cot. CitrusCane

O projeto Cotação CitrusCane consiste em transformar anos de cotações de laranja, limão e cana em dashboard. Para isso é utilizado crawlers que coletam
e amarzenam em um banco MongoDB Atlas e visualizado no metabase.
<br><br>
# Documentação
<br>[M.V.P](https://docs.google.com/presentation/d/1KEZPY8OU4Cq1SFRD9gIA4wKgcAo6_4eZWU0wmDdezD0/edit#slide=id.p)</br>
<br>[Mapa de Conhecimento](https://gitlab.com/BDAg/cot.-citruscane/-/wikis/Mapa-de-Conhecimento)</br>
<br>[Cronograma](https://docs.google.com/spreadsheets/d/1nKjCjHOuohYALQb2nPM23kde5CL_b0oX0ZorI5Tg67o/edit)</br>
<br>[Matriz de Habilidades](https://docs.google.com/spreadsheets/d/16O4OQMqqHpEHEZQhvyVcfcI0KORwuQI4TJrpMDcj6iY/edit?usp=sharing)</br>
<br>[Matriz de Habilidades - Final do Projeto](https://gitlab.com/BDAg/cot.-citruscane/-/wikis/Matriz-de-habilidades---Final-do-projeto)</br>
<br>[Lições Aprendidas](https://gitlab.com/BDAg/cot.-citruscane/-/wikis/Li%C3%A7%C3%B5es-Aprendidas)</br>
<br>[Apresentação Final](https://www.youtube.com/watch?v=Ts8ETx4at0w)</br>
<br>[Relatorio Final](https://gitlab.com/BDAg/cot.-citruscane/-/wikis/Relat%C3%B3rio-Final)</br>

# Equipe
<br>[Mateus Teodoro (Scrum Master)](https://www.linkedin.com/in/mateus-teodoro-vieira-baptista-78b865188/)</br>
<br>[Fabio de Genova](https://www.linkedin.com/in/fagenova/)</br>
<br>[Angelo Luiz da Silva]()</br>
<br>[Lucas Vilela](https://www.linkedin.com/in/lucas-vilela-de-oliveira-7b90131aa/)</br>
<br>[Mateus Scandolera](https://www.linkedin.com/in/mateus-s-sousa-9b3110197/)</br>


# Como utilizar o Crawler

Para rodar o crawler em seu computador é necessário configurar o ambiente com algumas bibliotecas, afim de garantir um funcionamento 100% satisfatório.
As configurações a seguir foram executadas em windows 10/ Ubuntu 16.04, e seguem o mesmo padrão para outras distribuições baseadas em Debian ou no próprio windows 10/ Ubuntu 16.04.

**Python** 

O código está estruturado em Python 3, que já vem instalado na versão 3.6.7 por padrão no windows 10. Para verificar se o Python 3 encontra-se instalado em seu computador, basta abrir um novo terminal (Control+Shift+T no teclado) e digitar:


```bash
python3 --version

```

O terminal também pode ser utilizado para rodar o crawler após a instalação das bibliotecas, como será detalhado no final.

**MongoDB**

As informações coletadas pelo crawler precisam ser armazenadas para que possam ser posteriormente utilizadas e analisadas. Para tanto, utilizamos o Banco de Dados Não Relacional (NOSql) MongoDB. Para criar uma conta online e inserir seus dados basta acessar a página: https://www.mongodb.com/

**Bibliotecas**

Como mencionado anteriormente, nosso código roda em Python 3 e para isso utilizaremos seu gerenciador de pacotes, o PIP. 
para instalar o PIP no Ubuntu, basta abrir o terminal e digitar:


```bash
sudo apt-get install python3-pip
```

para saber se a instalação ocorreu com sucesso, após os pacotes baixados basta inserir o comando:

```bash
pip3 --version
```

O resultado será semelhante a este:

`pip 9.0.1 from /usr/lib/python3/dist-packages (python 3.6)`

Após instalado o PIP, prosseguiremos com a instalação das bibliotecas necessárias. O Python já vem com algumas bibliotecas nativamente instaladas, como a datetime utilizada no projeto e precisaremos apenas daquelas que ainda não estão presentes:


bs4:


```bash
pip3 install bs4
```

requests:

```bash
pip3 install requests
```

lxml:


```bash
pip3 install lxml
```

pymongo:

```bash
pip3 install pymongo
pip3 install dnspython
```

Para verificar se as bibliotecas foram instaladas basta utilizar o comando:


```bash
pip3 list
```

Ao final da instalação das bibliotecas e criação da base de dados no MongoDB basta acessar a pasta em que o crawler se encontra através do terminal e executar o seguinte comando, substituindo a palavra "codigo" pelo nome do arquivo do crawler desejado:


```bash
python3 codigo.py
```



